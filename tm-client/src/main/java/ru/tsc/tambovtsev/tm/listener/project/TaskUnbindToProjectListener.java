package ru.tsc.tambovtsev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.dto.request.TaskUnbindProjectRequest;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

@Component
public final class TaskUnbindToProjectListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "unbind-task-from-project";

    @NotNull
    public static final String DESCRIPTION = "Unbind task from project.";

    @NotNull
    public static final String ARGUMENT = null;

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @EventListener(condition = "@taskUnbindToProjectListener.getName() == #event.name")
    public void handlerConsole(@NotNull final ConsoleEvent event) {
        @Nullable final String userId = getUserId();
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        getTaskEndpoint().unbindProjectTask(new TaskUnbindProjectRequest(userId, taskId));
    }

}
