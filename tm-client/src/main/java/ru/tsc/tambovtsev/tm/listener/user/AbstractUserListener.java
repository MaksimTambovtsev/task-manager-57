package ru.tsc.tambovtsev.tm.listener.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.tambovtsev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.tambovtsev.tm.listener.AbstractListener;

@Getter
@Setter
@Component
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpoint;

    @Nullable
    public String getArgument() {
        return null;
    }

}
